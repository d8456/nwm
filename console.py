from pickletools import read_float8
from turtle import back
from go import Board, Color
from colorama import Fore, Back, Style, init
import sys
import csv

def readFile(filename):
    with open(filename, newline='') as f:
        reader = csv.reader(f)
        data = [tuple(row) for row in reader]
        return data

def main(boardSize, filename=None):
    init()

    board = Board(boardSize)
    if filename != None:
        array = readFile(filename)
        if len(array) != boardSize:
            exit(19)
        board.init(array)    

    currColor = list(Color)[0]
    scoreboard = {colorObj: 0 for colorObj in list(Color)[:2]}
    while len(rowcol := input('Row col: ')) > 2:
        rowcol = rowcol.split()
        #TODO trycatch int
        row = int(rowcol[0])
        col = int(rowcol[1])
        scoreInc, success = board.makeMove(currColor, row, col)
        if success:
            scoreboard[currColor] += scoreInc
            _draw(board.getBoard(), boardSize)
            print(scoreboard)
            currColor = Color.getOpposite(currColor)
    print(scoreboard)

#masakra nie rozwijac
def _draw(board, boardSize):#TODO SRP
    background = Back.CYAN
    spaceSeq = "  "
    #print(Style.DIM,end='') not supported on windows by colorama
    initialHeaderPadding = ''
    for it in range(len(str(boardSize))-1):
        initialHeaderPadding += ' '
    print(spaceSeq + initialHeaderPadding + ' ',end='')#padding
    for it in range(boardSize):
        dynamicHeaderPadding = ''
        for itt in range(len(str(boardSize)) - len(str(it))+1):
            dynamicHeaderPadding += ' '
        print(str(it) + dynamicHeaderPadding,end='') 
    print()
    for rowN, row in enumerate(board):
        numberStr = str(rowN)
        #for column alignment, row counters can be 1/2/3/.. signs
        rowCounterPadding = ''
        for it in range(len(str(boardSize)) - len(numberStr)):
            rowCounterPadding += ' '
        print(numberStr + rowCounterPadding + spaceSeq, end='')
        print(background, end='')
        for cellN, cell in enumerate(row):
            if cell == Color.WHITE:
                char = '0'
                color = Fore.WHITE
            elif cell == Color.BLACK:
                char = '0'
                color = Fore.BLACK
            else:
                char = ' '
                color = background
            print(color + char + (spaceSeq if cellN <= boardSize-2 else ''),
             end = '')
        print(Style.RESET_ALL)

if __name__ == "__main__":
    if len(sys.argv) == 2:
        main(int(sys.argv[1]))
    elif len(sys.argv) == 3:
        main(int(sys.argv[1]), sys.argv[2])